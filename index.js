import { AppRegistry } from 'react-native';
import App from './transpiled/app';

AppRegistry.registerComponent('react_native_coffee_boilerplate', () => App);
